[comment {-*- tcl -*- doctools manpage}]
[vset libname dbus-tcl]
[manpage_begin [vset libname] n 1.0]
[copyright {2008-2010 Schelte Bron, Alexander Galanin}]
[moddesc   {Tcl DBus extension}]
[titledesc {Tcl library for interacting with the DBus}]
[require Tcl 8.5]
[require dbus-tcl 1.0]
[description]

The [package [vset libname]] package provides commands to interact with DBus
message busses. There are three well-known bus names: [const session],
[const system], and [const starter]. The starter bus only applies when the
application has been started as a result of a method call from another
application. A connection to the starter bus will effectively be an
alternative connection to either the session or the system bus.

[para]

Most subcommands take a [arg busID] argument. This is the DBus handle as
returned by the [cmd dbus] [method connect] subcommand. For the well-known
busses the handle matches the name of the bus. If the [arg busID] argument
is not specified, it defaults to [const session].

[list_begin definitions]

[call [cmd dbus] [method call] [opt [arg busID]] \
  [opt "[option -autostart] [arg boolean]"] \
  [opt "[option -dest] [arg target]"] \
  [opt "[option -handler] [arg script]"] \
  [opt "[option -signature] [arg string]"] \
  [opt "[option -timeout] [arg ms]"] \
  [arg path] [arg interface] [arg method] \
  [opt "[arg arg] [arg ...]"] \
]

Send a method call onto the dbus and optionally wait for a reply.

If the signature of the DBus [const method_return] reply only contains one
top-level element, the arguments of the [const method_return] message are
returned as a single value. More complex structures are returned as a list.

[para]

If the response to the DBus [const method_call] message is a DBus
[const error] message, the command will produce an error. In that case the
errorCode variable will be set to [const DBUS] [const DBUS_MESSAGE_TYPE_ERROR].

[para]

The [option -autostart] option specifies whether the bus server should
attempt to start an associated application if the destination name does not
currently exist on the bus. [arg Boolean] may have any proper boolean value,
such as [const 1] or [const no]. Defaults to 1 (true).

[para]

The [option -timeout] option specifies the maximum time to wait for a
response. A negative timeout indicates that no response should be requested. 

[para]

If a script is specified with the [option -handler] option, the call will be
asynchronous. In that case the command returns the serial of the request.
The script will be executed when a response comes back or when there is an
error.

[para]

The [option -signature] option defines the types of arguments to be sent on
the dbus. See the [sectref Signatures] section for more information.
If no signature is specified, all arguments will be sent as strings.

[call [cmd dbus] [method close] [opt [arg busID]]]

Close the connection to the DBus. This will cleanup all handlers, listeners,
and the optional monitor script registered for the [arg busID]. The actual
bus connection of the application will not really be terminated if [arg busID]
is one of the three so-called well-known busses, as this is not supported by
libdbus. Reconnecting to one of those busses will result in reusing the same
unique name as before.

[call [cmd dbus] [method connect] [opt [arg address]]]

Connect to the DBus. The [arg address] argument specifies the bus to connect
to. This can be either one of the well-known busses ('session', 'system' or
'starter'), or a transport name followed by a colon, and then an optional,
comma-separated list of keys and values in the form key=value.
The command returns a handle that can be used as the [arg busID] argument
in other dbus commands.
It is legal to run this command when already connected.

[call [cmd dbus] [method error] [opt [arg busID]] \
  [opt "[option -name] [arg string]"] \
  [arg destination] [arg serial] \
  [opt message] \
]

Send a DBus [const error] message with the specified [arg serial] and
[arg destination]. If the [option -name] option is not specified, it
defaults to "org.freedesktop.DBus.Error.Failed".

This method is targetted to be used in combination with a script registered
using the [method dbus] [method method] subcommand with the [option -async]
option.

The values for [arg destination] and [arg serial] can be obtained from event
information fields 'sender' and 'serial' correspondingly.
See [sectref "Event Handlers"] below for more information.

[call [cmd dbus] [method filter] [opt [arg busID]] [arg subcommand] \
  [option -option] [arg value] [opt [arg ...]] \
]

The [method add] subcommand adds a match rule to match messages going through
the message bus. The [method remove] subcommand removes the most recently
added rule that exactly matches the specified option settings. If there is no
matching rule, the command is silently ignored. Available options are:
[option -destination], [option -interface], [option -member], [option -path],
[option -sender], and [option -type].
The command returns the match rule passed to libdbus.

[call [cmd dbus] [method info] [opt [arg busID]] [arg option]]

The info command can be used to obtain information about the DBus.
Available [method info] options are:
[list_begin commands]
[cmd_def [method machineid]] Get the UUID of the local machine.
[cmd_def [method local]] The object path used in local/in-process-generated
messages ([const /org/freedesktop/DBus/Local]).
[cmd_def [method path]] The object path used to talk to the bus itself
([const /org/freedesktop/DBus]).
[cmd_def [method pending]] Report if any messages are in the queue to be sent.
[cmd_def [method serverid]] Get the UUID of the server we are authenticated to.
[cmd_def [method service]] The bus name used to talk to the bus itself
([const org.freedesktop.DBus]).
[cmd_def [method version]]Returns the version of libdbus.
[list_end]
[para]

[call [cmd dbus] [method listen] [opt [arg busID]] \
[opt "[arg path] [opt "[arg member] [opt [arg script]]"]"]]

Register a [arg script] to be called when the signal named "[arg member]" at
[arg path] appears on the DBus.
See [sectref "Event Handlers"] below for more information.

[para]

If the [arg path] argument is an empty string, [arg script] will be executed
whenever a signal message is received for any path, unless a dedicated
listener for the exact path has been defined.
The [arg member] argument may be specified as either a signal name or an
interface and signal name joined by a period. If no interface is specified,
the script will be called for signals with any interface.

[para]

If [arg script] is an empty string, the currently registered command for the
specified signal and path will be unregistered.

If the [arg script] argument is not specified, the currently registered
command for the specified signal and path, if any, is returned.
If no [arg member] argument is specified a list of all registered signals
and associated commands at the specified path is returned.
If no [arg path] argument is specified a list of all paths and their
registered signals and associated commands is returned.

[call [cmd dbus] [method method] [opt [arg busID]] [opt [option -async]] \
[opt "[arg path] [opt "[arg member] [opt [arg script]]"]"]]

Register a [arg script] to be called when method [arg member] is invoked at
the specified [arg path].
See [sectref "Event Handlers"] below for more information.

[para]

If the [arg path] argument is an empty string, [arg script] will be executed
whenever a method call message is received for any path, unless a dedicated
method handler for the exact path has been defined.
The [arg member] argument may be specified as either a method name or an
interface and method name joined by a period. If no interface is specified,
the script will be called for methods with any interface, unless another
handler is specified for the method including the interface.

If [arg script] is an empty string, the currently registered command for the
specified method and path will be unregistered.

[para]

When a [arg script] argument is specified, even if it is an empty string,
the command may fail if another interpreter has already registered a handler
for the exact same path, interface and method.
See [sectref "Slave Interpreters"] below for more information.

[para]

If the [arg script] argument is not specified, the currently registered
command for the specified method and path, if any, is returned. 

If no [arg member] argument is specified a list of all registered methods
and associated commands at the specified path is returned.

If no [arg path] argument is specified a list of all paths and their
registered methods and associated commands is returned.

[para]

In the simple case, when [arg script] is evaluated because of a
[const method_call], the result of the script will be returned to the caller
as a string in a DBus [const method_return] message. If the execution of
[arg script] ends with an error, the error message is returned to the caller
in a DBus [const error] message. Any DBus errors that happen while sending
these messages back to the caller are silently ignored. If the caller
specified the [const no_reply] flag in the [const method_call] as FALSE, no
[const method_return] or [const error] message will be returned. 

[para]

For more advanced control over the returned messages the [option -async]
option can be specified while registering the script. First of all, as the
name suggests, this option allows results or errors to be returned from code
outside of [arg script]. The [cmd dbus] [method return] and [cmd dbus]
[method error] subcommands should be used for that.

The [cmd dbus] [method return] subcommand can also be used to return more
complex data structures than a string from [arg script]. In that case the
[option -async] option ensures that the return value of [arg script] is
suppressed.

Even when returning a string to the caller from within [arg script] it may
still be useful to specify the [option -async] option and use the [cmd dbus]
[method return] subcommand. This allows handling of dbus errors while
sending back the return message.

[para]

When evaluation of [arg script] ends in an error, the error will always be
returned to the caller unless the [const no_reply] flag was set to TRUE. The
setting of the [option -async] option has no influence on this behaviour.

[call [cmd dbus] [method monitor] [opt [arg busID]] [arg script]]

Register a [arg script] to be executed when any DBus message is received.

See [sectref "Event Handlers"] below for more information.

[para]

This can be useful for building special purpose programs that need to see
all activity on the DBus, for example a DBus monitoring program. 
If [arg script] is an empty string, the currently configured monitor script
will be removed.

[call [cmd dbus] [method name] [opt [arg busID]] \
  [opt "[option -option] [arg ...]"] [arg name]
]

Request the bus to assign a given name to the connection. The command will
generate an error in all cases where it was unsuccessful in making the
application the primary owner of the name. 

[para]

The [option -yield] option specifies that the application will release the
requested name when some other application requests the same name and has
indicated that it wants to take over ownership of the name. The application
will be informed by a signal when it loses ownership of the name.

[para]

The [option -replace] option indicates that the application wants to take
over the ownership of the name from the application that is currently the
primary owner, if any. This request will only be honoured if the current
owner has indicated that it will release the name on request.
See also the [option -yield] option.

[para]

If the requested name is currently in use and the [option -replace] option
has not been specified, or the [option -replace] option was specified but
the current owner is unwilling to give up its ownership, the name request
will normally be queued. Then when the name is released by current owner it
is assigned to the next requester in the queue and a signal is sent to inform
that requester that it is now the primary owner of the name.

The [option -noqueue] option may be specified to indicate that the name
request should not be queued.

[para]

Note that even if the request has been queued, the command will generate an
error because the goal of becoming the primary owner of the name has not
been achieved.

[call [cmd dbus] [method release] [opt [arg busID]] [arg name]]

Asks the bus to unassign the given name from this connection.

[call [cmd dbus] [method return] [opt [arg busID]] \
  [opt "[option -signature] [arg string]"] \
  [arg destination] [arg serial] \
  [opt "[arg arg] [arg ...]"] \
]

Send a DBus [const method_return] message with the specified [arg serial] and
[arg destination]. This method is targetted to be used in combination with
a script registered using the [method dbus] [method method] subcommand
with the [option -async] option.
The values for [arg destination] and [arg serial] can be obtained from event
information fields 'sender' and 'serial' correspondingly. 
See [sectref "Event Handlers"] below for more information.

[call [cmd dbus] [method signal] [opt [arg busID]] \
  [opt "[option -signature] [arg string]"] \
  [arg object] [arg interface] [arg name] \
  [opt "[arg arg] [arg ...]"] \
]

Send a signal onto the dbus with the specified type signature. If no
[option -signature] option is provided, all [arg arg]s will be sent as
strings. The command returns the serial number of the dbus message.

[call [cmd dbus] [method validate] [arg class] [arg string]]

Validates [arg string] against the rules of the D-Bus specification for
the type of value specified by [arg class]. Returns 1 if validation passes,
otherwise returns 0. The following classes are recognized (the class name
can be abbreviated):

[list_begin definitions]
[def [const interface]] Two or more dot-separated non-empty elements.
Each element only contains the ASCII characters 
"[lb]A-Z[rb][lb]a-z[rb][lb]0-9[rb]_" and does not begin with a digit.

[def [const member]] A string that only contains the ASCII characters
"[lb]A-Z[rb][lb]a-z[rb][lb]0-9[rb]_" and does not begin with a digit.

[def [const name]] Either a unique connection name, or a well-known
connection name. Unique connection names begin with a colon and consist of
at least two dot-separated non-empty elements. Each element only contains
the ASCII characters "[lb]A-Z[rb][lb]a-z[rb][lb]0-9[rb]_".
Well-known connection names consist of at least two dot-separated
non-empty elements. Each element only contains the ASCII characters
"[lb]A-Z[rb][lb]a-z[rb][lb]0-9[rb]_" and does not begin with a digit.

[def [const path]] A slash followed by zero or more slash-separated 
non-empty elements. Each element only contains the ASCII characters
"[lb]A-Z[rb][lb]a-z[rb][lb]0-9[rb]_".

[def [const signature]] A valid D-Bus message type signature. See
[sectref Signatures] below for more information on what constitutes a
valid signature.
[list_end]
[para]

[list_end]

[section "Event Handlers"]
The [method call], [method listen], [method method] and [method monitor]
methods provide the ability to define event handlers. The specified script
will be used as the prefix for a command that will be evaluated whenever
the corresponding DBus event occurs. When the DBus event occurs, a Tcl
command will be generated by concatenating the script with one or more
arguments. The first argument is a dict containing information about the
event. If the DBus event contained any arguments they will be appended to
the command as seperate arguments.

[para]
The dict with the event details contains the following information:
[list_begin definitions]
[def member] The interface member being invoked (for methods) or emitted
(for signals).
[def interface] The interface this message is being sent to (for methods)
or being emitted from (for signals). The interface name is fully-qualified.
[def path] The object path this message is being sent to (for methods) or
being emitted from (for signals).
[def sender] The unique name of the connection which originated this message,
or the empty string if unknown or inapplicable. The sender is filled in by
the message bus. Note, the returned sender is always the unique bus name.
Connections may own multiple other bus names, but those are not found in the
sender field.
[def destination] The destination of a message or the empty string if there
is none set.
[def messagetype] The type of a message. Possible values are
[const method_call], [const method_return], [const error], and [const signal].
[def signature] The type signature of the message, i.e. the type specification
of the arguments in the message payload. See [sectref Signatures] below for
more information.
[def serial] The serial of a message or 0 if none has been specified. The
message's serial number is provided by the application sending the message
and is used to identify replies to this message. All messages received on a
connection will have a serial provided by the remote application. When
sending messages a serial will automatically be assigned by the [vset libname]
library.
[def replyserial] The serial that the message is a reply to or 0 if none.
[def noreply] Flag indicating if the sender expects a reply. Set to 1 if a
reply is [emph not] required.
[def autostart] Flag indicating if the message will cause an owner for
destination name to be auto-started.
[def errorname] The error name of a received error message. An empty string
for all other message types.
[list_end]

The event handlers are excuted at global level (outside the context of any
Tcl procedure) in the interpreter in which the event handler was installed.

[section Signatures]
The DBus specification defines typed arguments. This doesn't fit well with
the Tcl philosophy of [term "everything is a string"]. To be able to closely
control the type of the arguments to be sent onto the DBus a [const signature]
can be supplied. The signature definition is exactly the same as in the DBus
specification. A signature is a string where a single character or group of
characters specifies the type of an argument. The following types exist:
[list_begin definitions]
[def s] A UTF-8 encoded, nul-terminated Unicode string.
[def b] A boolean, FALSE (0), or TRUE (1).
[def y] A byte (8-bit unsigned integer).
[def n] A 16-bit signed integer.
[def q] A 16-bit unsigned integer.
[def i] A 32-bit signed integer.
[def u] A 32-bit unsigned integer.
[def x] A 64-bit signed integer.
[def t] A 64-bit unsigned integer.
[def d] An 8-byte double in IEEE 754 format.
[def g] A type signature.
[def o] An object path.
[def a#] A D-Bus array type, which is similar to a Tcl list. The # specifies
the type of the array elements. This can be any type, including another
array, a struct or a dict entry.
[def v] A D-Bus variant type. The provided value should be a two-element list,
containing a signature and the actual value. See [sectref "Variant Arguments"]
for more information.
[def (...)] A struct. The string inside the parentheses defines the types of
the arguments within the struct, which may consist of a combination of any
of the existing types.
[def {{##}}] A dict entry. Dict entries may only occur as array elements. The
first # specifies the type of the dict key. This must be a basic type
(one of 'sbynqiuxtdgo'). The second # specifies the type of the dict value.
This can again be any existing type.
[list_end]
[emph Example]: The signature 'vaas(id)a{i(ss)}' specifies four arguments and
translates to Tcl terminology as follows: The type of the first argument
('v') is extracted from its value. The second argument ('aas') is a list
containing lists of strings. The third argument ('(id)') is a list containing
an integer and a double. The last argument ('a{i(ss)}') is a dict (an array
of dict entries) with integer keys and each value is a list of two strings.

[subsection "Variant Arguments"]

[comment http://www.galago-project.org/specs/notification/0.9/x344.html]

When a signature specifies that an argument is of type "variant", the
argument can still be almost anything. Additional information needs to be
located to determine how to package the value for transmission onto the
dbus. The preferred way is for the argument to be a two-element list where
the first element specifies the signature for the value and the second
element is the actual value.

[para]

The signature for a variant argument has to specify a single complete type.
The value of the second list element must match the signature, otherwise an
error will be reported.

[para]

If the value provided for a variant argument is not a two-element list, or
the first element is not a valid signature for a single complete type, the
code will attempt to automatically determine the type of the provided value.
It does this by selecting a signature based on the internal representation
of the value according to the following table:
[list_begin definitions]
[def "string: s"]
[def "int: i"]
[def "wideInt: x"]
[def "double: d"]
[def "boolean: b"]
[def "list: as"]
[def "dict: a{ss}"]
[def "anything else: s"]
[list_end]

[section "Slave Interpreters"]
The standard D-Bus library [const libdbus] will only assign a single unique
bus name per application. This means that slave interpreters that connect to
the D-Bus will get the same unique bus name as the main interpreter, or any
other slave interpreter that has connected to the D-Bus.

[para]

It is not a problem if multiple interpreters register a listener for the
exact same signal. The [package [vset libname]] package will execute the
commands for all interpreters (in an undefined order). The same applies to
monitor commands registered by different interpreters. However, a method
call generally causes a result being returned to the caller. Therefor there
should only be exactly one handler registered for a specific method. If any
interpreter tries to register a method handler for an interface and member
at a path that is already registered by another interpreter, the request
will be denied.

[section "Error Messages"]
In addition to the regular errors that may occur when loading any package,
[package [vset libname]] may fail to load with the following error messages:
[list_begin definitions]
[def [emph "could not load dbus library"]]
The package failed to dynamically load the libdbus-1 shared library. Most
likely the library is not installed on the system.
[def [emph "dbus library does not support dbus_get_version"]]
The libdbus-1 shared library found on the system does not support the
dbus_get_version call. This means the library is too old and should be
upgraded to be able to use [package [vset libname]].
[def [emph "wrong dbus library version - found x.y.z, need at least 1.2.1"]]
The libdbus-1 shared library found on the system has the wrong version.
The library should be upgraded to be able to use [package [vset libname]].
[list_end]
[manpage_end]
