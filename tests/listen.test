if {[lsearch [namespace children] ::tcltest] == -1} {
    package require tcltest 2
    namespace import -force ::tcltest::*
}

package require dbus-tcl
dbus connect

proc handler {data args} {
    global signal
    set signal [dict get $data member]
}

test listen-1.1 {simple listener} -body {
    catch {dbus listen / foo dummy}
} -cleanup {
    dbus listen / foo {}
} -result 0

test listen-1.2 {query all handlers} -setup {
    dbus listen / foo dummy
    dbus listen /foo bar dummy2
} -body {
    dbus listen
} -cleanup {
    dbus listen / foo {}
    dbus listen /foo bar {}
} -result [list / foo dummy /foo bar dummy2]

test listen-1.3 {query handlers for a path} -setup {
    dbus listen / foo dummy
    dbus listen /foo bar dummy2
} -body {
    dbus listen /
} -cleanup {
    dbus listen / foo {}
    dbus listen /foo bar {}
} -result [list / foo dummy]

test listen-1.4 {query listen handler} -setup {
    dbus listen / foo dummy
    dbus listen /foo bar dummy2
} -body {
    dbus listen / foo
} -cleanup {
    dbus listen / foo {}
    dbus listen /foo bar {}
} -result dummy

test listen-1.5 {unregistering handler} -body {
    dbus listen / foo {}
} -result {}

test listen-1.6 {register handler for invalid path} -body {
    dbus listen /test/ foo dummy
} -returnCodes error -result {Invalid path}

test listen-1.7 {register handler with empty path} -body {
    dbus listen {} foo dummy
} -cleanup {
    dbus listen {} foo {}
} -result {}

test listen-1.8 {check signal operation} -setup {
    dbus listen /test com.tclcode.test.testsignal handler
    dbus filter add -type signal -interface com.tclcode.test
} -body {
    dbus call -dest com.tclcode.test.responder /test com.tclcode.test signal
    after 200 {set signal timeout}
    vwait signal
    return $signal
} -cleanup {
    dbus filter remove -type signal -interface com.tclcode.test
    dbus listen /test com.tclcode.test.testsignal {}
} -result {testsignal}

test listen-1.9 {check signal operation with empty path} -setup {
    dbus listen {} com.tclcode.test.testsignal handler
    dbus filter add -type signal -interface com.tclcode.test
} -body {
    dbus call -dest com.tclcode.test.responder /test com.tclcode.test signal
    after 200 {set signal timeout}
    vwait signal
    return $signal
} -cleanup {
    dbus filter remove -type signal -interface com.tclcode.test
    dbus listen {} com.tclcode.test.testsignal {}
} -result {testsignal}

# cleanup
cleanupTests
return
