#include "dbustcl.h"

Tcl_HashTable bus;
Tcl_DBusBus *defaultbus = NULL;
static int initialized = 0;
static void *handle = NULL;
TCL_DECLARE_MUTEX(dbusMutex)

/* dbus-bus.h */
DBusConnection* (*pdbus_bus_get)(DBusBusType, DBusError*);
dbus_bool_t (*pdbus_bus_register)(DBusConnection*, DBusError*);
const char* (*pdbus_bus_get_unique_name)(DBusConnection*);
int (*pdbus_bus_request_name)(DBusConnection*, const char*, unsigned int, DBusError*);
int (*pdbus_bus_release_name)(DBusConnection*, const char*, DBusError*);
void (*pdbus_bus_add_match)(DBusConnection*, const char*, DBusError*);
void (*pdbus_bus_remove_match)(DBusConnection*, const char*, DBusError*);

/* dbus-connection.h */
DBusConnection* (*pdbus_connection_open)(const char*, DBusError*);
void (*pdbus_connection_unref)(DBusConnection*);
char* (*pdbus_connection_get_server_id)(DBusConnection*);
void (*pdbus_connection_flush)(DBusConnection*);
dbus_bool_t (*pdbus_connection_read_write)(DBusConnection*, int);
DBusDispatchStatus (*pdbus_connection_get_dispatch_status)(DBusConnection*);
DBusDispatchStatus (*pdbus_connection_dispatch)(DBusConnection*);
dbus_bool_t (*pdbus_connection_has_messages_to_send)(DBusConnection*);
dbus_bool_t (*pdbus_connection_send)(DBusConnection*, DBusMessage*, dbus_uint32_t*);
dbus_bool_t (*pdbus_connection_send_with_reply)(DBusConnection*, DBusMessage*, DBusPendingCall**, int);
dbus_bool_t (*pdbus_connection_set_timeout_functions)(DBusConnection*, DBusAddTimeoutFunction, DBusRemoveTimeoutFunction, DBusTimeoutToggledFunction, void*, DBusFreeFunction);
dbus_bool_t (*pdbus_connection_register_object_path)(DBusConnection*, const char*, const DBusObjectPathVTable*, void*);
dbus_bool_t (*pdbus_connection_register_fallback)(DBusConnection*, const char*, const DBusObjectPathVTable*, void*);
dbus_bool_t (*pdbus_connection_unregister_object_path)(DBusConnection*, const char*);
dbus_bool_t (*pdbus_connection_get_object_path_data)(DBusConnection*, const char*, void**);
dbus_bool_t (*pdbus_connection_list_registered)(DBusConnection*, const char*, char***);
int (*pdbus_timeout_get_interval)(DBusTimeout*);
void* (*pdbus_timeout_get_data)(DBusTimeout*);
void (*pdbus_timeout_set_data)(DBusTimeout*, void*, DBusFreeFunction);
dbus_bool_t (*pdbus_timeout_handle)(DBusTimeout*);

/* dbus-errors.h */
void (*pdbus_error_init)(DBusError*);
void (*pdbus_error_free)(DBusError*);
dbus_bool_t (*pdbus_error_is_set)(const DBusError*);

/* dbus-message.h */
DBusMessage* (*pdbus_message_new)(int);
DBusMessage* (*pdbus_message_new_method_call)(const char*, const char*, const char*, const char*);
DBusMessage* (*pdbus_message_ref)(DBusMessage*);
void (*pdbus_message_unref)(DBusMessage*);
int (*pdbus_message_get_type)(DBusMessage*);
dbus_bool_t (*pdbus_message_set_path)(DBusMessage*, const char*);
const char* (*pdbus_message_get_path)(DBusMessage*);
dbus_bool_t (*pdbus_message_set_interface)(DBusMessage*, const char*);       
const char* (*pdbus_message_get_interface)(DBusMessage*);
dbus_bool_t (*pdbus_message_set_member)(DBusMessage*, const char*);
const char* (*pdbus_message_get_member)(DBusMessage*);
dbus_bool_t (*pdbus_message_set_error_name)(DBusMessage*, const char*);
const char* (*pdbus_message_get_error_name)(DBusMessage*);
dbus_bool_t (*pdbus_message_set_destination)(DBusMessage*, const char*);
const char* (*pdbus_message_get_destination)(DBusMessage*);
dbus_bool_t (*pdbus_message_set_sender)(DBusMessage*, const char*);
const char* (*pdbus_message_get_sender)(DBusMessage*);
const char* (*pdbus_message_get_signature)(DBusMessage*);
void (*pdbus_message_set_no_reply)(DBusMessage*, dbus_bool_t);
dbus_bool_t (*pdbus_message_get_no_reply)(DBusMessage*);
dbus_uint32_t (*pdbus_message_get_serial)(DBusMessage*);
void (*pdbus_message_set_serial)(DBusMessage*, dbus_uint32_t);
dbus_bool_t (*pdbus_message_set_reply_serial)(DBusMessage*, dbus_uint32_t);
dbus_uint32_t (*pdbus_message_get_reply_serial)(DBusMessage*);
void (*pdbus_message_set_auto_start)(DBusMessage*, dbus_bool_t);
dbus_bool_t (*pdbus_message_get_auto_start)(DBusMessage*);
dbus_bool_t (*pdbus_message_append_args)(DBusMessage*, int, ...);
dbus_bool_t (*pdbus_message_iter_init)(DBusMessage*, DBusMessageIter*);
dbus_bool_t (*pdbus_message_iter_next)(DBusMessageIter*);
char* (*pdbus_message_iter_get_signature)(DBusMessageIter*);
int (*pdbus_message_iter_get_arg_type)(DBusMessageIter*);
int (*pdbus_message_iter_get_element_type)(DBusMessageIter*);
void (*pdbus_message_iter_recurse)(DBusMessageIter*, DBusMessageIter*);
void (*pdbus_message_iter_get_basic)(DBusMessageIter*, void*);
void (*pdbus_message_iter_get_fixed_array)(DBusMessageIter*, void*, int*);
void (*pdbus_message_iter_init_append)(DBusMessage*, DBusMessageIter*);
dbus_bool_t (*pdbus_message_iter_append_basic)(DBusMessageIter*, int, const void*);
dbus_bool_t (*pdbus_message_iter_open_container)(DBusMessageIter*, int, const char*, DBusMessageIter*);
dbus_bool_t (*pdbus_message_iter_close_container)(DBusMessageIter*, DBusMessageIter*);
const char* (*pdbus_message_type_to_string)(int);

/* dbus-misc.h */
char* (*pdbus_get_local_machine_id)(void);
void (*pdbus_get_version)(int*, int*, int*);

/* dbus-memory.h */
void (*pdbus_free)(void*);
void (*pdbus_free_string_array)(char**);

/* dbus-signature.h */
void (*pdbus_signature_iter_init)(DBusSignatureIter*, const char*);
int (*pdbus_signature_iter_get_current_type)(const DBusSignatureIter*);
char* (*pdbus_signature_iter_get_signature)(const DBusSignatureIter*);
dbus_bool_t (*pdbus_signature_iter_next)(DBusSignatureIter*);
void (*pdbus_signature_iter_recurse)(const DBusSignatureIter*, DBusSignatureIter*);
dbus_bool_t (*pdbus_signature_validate)(const char*, DBusError*);
dbus_bool_t (*pdbus_signature_validate_single)(const char*, DBusError*);
dbus_bool_t (*pdbus_type_is_fixed)(int);

/* dbus-pending-call.h */
void (*pdbus_pending_call_unref)(DBusPendingCall*);
dbus_bool_t (*pdbus_pending_call_set_notify)(DBusPendingCall*, DBusPendingCallNotifyFunction, void*, DBusFreeFunction);
DBusMessage* (*pdbus_pending_call_steal_reply)(DBusPendingCall*);
void (*pdbus_pending_call_block)(DBusPendingCall*);

int DBus_Load(Tcl_Interp *interp)
{
   int major, minor, micro;

   handle = dlopen("libdbus-1" SHLIB_SUFFIX, RTLD_NOW);
   if (handle == NULL) {
      Tcl_SetResult(interp, "could not load dbus library", NULL);
      return TCL_ERROR;
   }
   pdbus_get_version = dlsym(handle, "dbus_get_version");
   if (pdbus_get_version == NULL) {
      Tcl_SetResult(interp, "dbus library does not support "
		    "dbus_get_version", NULL);
      return TCL_ERROR;
   }
   (*pdbus_get_version)(&major, &minor, &micro);
   /* Check libdbus version */
   if (major < REQMAJOR || (major == REQMAJOR && minor < REQMINOR) ||
       (major == REQMAJOR && minor == REQMINOR && micro < REQMICRO)) {
      Tcl_SetObjResult(interp, Tcl_ObjPrintf("wrong dbus library version "
	"- found %d.%d.%d, need at least %d.%d.%d",
	major, minor, micro, REQMAJOR, REQMINOR, REQMICRO));
      return TCL_ERROR;
   }

   /* Lookup all remaining libdbus functions used by the package */
   pdbus_bus_get = 
     dlsym(handle, "dbus_bus_get");
   pdbus_bus_register = 
     dlsym(handle, "dbus_bus_register");
   pdbus_bus_get_unique_name = 
     dlsym(handle, "dbus_bus_get_unique_name");
   pdbus_bus_request_name = 
     dlsym(handle, "dbus_bus_request_name");
   pdbus_bus_release_name = 
     dlsym(handle, "dbus_bus_release_name");
   pdbus_bus_add_match = 
     dlsym(handle, "dbus_bus_add_match");
   pdbus_bus_remove_match = 
     dlsym(handle, "dbus_bus_remove_match");
   pdbus_connection_open = 
     dlsym(handle, "dbus_connection_open");
   pdbus_connection_unref = 
     dlsym(handle, "dbus_connection_unref");
   pdbus_connection_get_server_id = 
     dlsym(handle, "dbus_connection_get_server_id");
   pdbus_connection_flush = 
     dlsym(handle, "dbus_connection_flush");
   pdbus_connection_read_write = 
     dlsym(handle, "dbus_connection_read_write");
   pdbus_connection_get_dispatch_status = 
     dlsym(handle, "dbus_connection_get_dispatch_status");
   pdbus_connection_dispatch = 
     dlsym(handle, "dbus_connection_dispatch");
   pdbus_connection_has_messages_to_send = 
     dlsym(handle, "dbus_connection_has_messages_to_send");
   pdbus_connection_send =
     dlsym(handle, "dbus_connection_send");
   pdbus_connection_send_with_reply = 
     dlsym(handle, "dbus_connection_send_with_reply");
   pdbus_connection_set_timeout_functions = 
     dlsym(handle, "dbus_connection_set_timeout_functions");
   pdbus_connection_register_object_path = 
     dlsym(handle, "dbus_connection_register_object_path");
   pdbus_connection_register_fallback = 
     dlsym(handle, "dbus_connection_register_fallback");
   pdbus_connection_unregister_object_path = 
     dlsym(handle, "dbus_connection_unregister_object_path");
   pdbus_connection_get_object_path_data = 
     dlsym(handle, "dbus_connection_get_object_path_data");
   pdbus_connection_list_registered = 
     dlsym(handle, "dbus_connection_list_registered");
   pdbus_timeout_get_interval =
     dlsym(handle, "dbus_timeout_get_interval");
   pdbus_timeout_get_data =
     dlsym(handle, "dbus_timeout_get_data");
   pdbus_timeout_set_data =
     dlsym(handle, "dbus_timeout_set_data");
   pdbus_timeout_handle =
     dlsym(handle, "dbus_timeout_handle");
   pdbus_error_init =
     dlsym(handle, "dbus_error_init");
   pdbus_error_free =
     dlsym(handle, "dbus_error_free");
   pdbus_error_is_set =
     dlsym(handle, "dbus_error_is_set");
   pdbus_message_new =
     dlsym(handle, "dbus_message_new");
   pdbus_message_new_method_call = 
     dlsym(handle, "dbus_message_new_method_call");
   pdbus_message_ref =
     dlsym(handle, "dbus_message_ref");
   pdbus_message_unref =
     dlsym(handle, "dbus_message_unref");
   pdbus_message_get_type =
     dlsym(handle, "dbus_message_get_type");
   pdbus_message_set_path =
     dlsym(handle, "dbus_message_set_path");
   pdbus_message_get_path =
     dlsym(handle, "dbus_message_get_path");
   pdbus_message_set_interface =
     dlsym(handle, "dbus_message_set_interface");
   pdbus_message_get_interface =
     dlsym(handle, "dbus_message_get_interface");
   pdbus_message_set_member =
     dlsym(handle, "dbus_message_set_member");
   pdbus_message_get_member =
     dlsym(handle, "dbus_message_get_member");
   pdbus_message_set_error_name = 
     dlsym(handle, "dbus_message_set_error_name");
   pdbus_message_get_error_name =
     dlsym(handle, "dbus_message_get_error_name");
   pdbus_message_set_destination =
     dlsym(handle, "dbus_message_set_destination");
   pdbus_message_get_destination = 
     dlsym(handle, "dbus_message_get_destination");
   pdbus_message_set_sender =
     dlsym(handle, "dbus_message_set_sender");
   pdbus_message_get_sender =
     dlsym(handle, "dbus_message_get_sender");
   pdbus_message_get_signature =
     dlsym(handle, "dbus_message_get_signature");
   pdbus_message_set_no_reply =
     dlsym(handle, "dbus_message_set_no_reply");
   pdbus_message_get_no_reply =
     dlsym(handle, "dbus_message_get_no_reply");
   pdbus_message_get_serial =
     dlsym(handle, "dbus_message_get_serial");
   pdbus_message_set_serial =
     dlsym(handle, "dbus_message_set_serial");
   pdbus_message_set_reply_serial =
     dlsym(handle, "dbus_message_set_reply_serial");
   pdbus_message_get_reply_serial =
     dlsym(handle, "dbus_message_get_reply_serial");
   pdbus_message_set_auto_start =
     dlsym(handle, "dbus_message_set_auto_start");
   pdbus_message_get_auto_start = 
     dlsym(handle, "dbus_message_get_auto_start");
   pdbus_message_append_args =
     dlsym(handle, "dbus_message_append_args");
   pdbus_message_iter_init =
     dlsym(handle, "dbus_message_iter_init");
   pdbus_message_iter_next =
     dlsym(handle, "dbus_message_iter_next");
   pdbus_message_iter_get_signature =
     dlsym(handle, "dbus_message_iter_get_signature");
   pdbus_message_iter_get_arg_type =
     dlsym(handle, "dbus_message_iter_get_arg_type");
   pdbus_message_iter_get_element_type = 
     dlsym(handle, "dbus_message_iter_get_element_type");
   pdbus_message_iter_recurse =
     dlsym(handle, "dbus_message_iter_recurse");
   pdbus_message_iter_get_basic =
     dlsym(handle, "dbus_message_iter_get_basic");
   pdbus_message_iter_get_fixed_array =
     dlsym(handle, "dbus_message_iter_get_fixed_array");
   pdbus_message_iter_init_append =
     dlsym(handle, "dbus_message_iter_init_append");
   pdbus_message_iter_append_basic =
     dlsym(handle, "dbus_message_iter_append_basic");
   pdbus_message_iter_open_container =
     dlsym(handle, "dbus_message_iter_open_container");
   pdbus_message_iter_close_container =
     dlsym(handle, "dbus_message_iter_close_container");
   pdbus_message_type_to_string = 
     dlsym(handle, "dbus_message_type_to_string");
   pdbus_get_local_machine_id =
     dlsym(handle, "dbus_get_local_machine_id");
   pdbus_free =
     dlsym(handle, "dbus_free");
   pdbus_free_string_array =
     dlsym(handle, "dbus_free_string_array");
   pdbus_signature_iter_init =
     dlsym(handle, "dbus_signature_iter_init");
   pdbus_signature_iter_get_current_type = 
     dlsym(handle, "dbus_signature_iter_get_current_type");
   pdbus_signature_iter_get_signature =
     dlsym(handle, "dbus_signature_iter_get_signature");
   pdbus_signature_iter_next =
     dlsym(handle, "dbus_signature_iter_next");
   pdbus_signature_iter_recurse =
     dlsym(handle, "dbus_signature_iter_recurse");
   pdbus_signature_validate =
     dlsym(handle, "dbus_signature_validate");
   pdbus_signature_validate_single = 
     dlsym(handle, "dbus_signature_validate_single");
   pdbus_type_is_fixed =
     dlsym(handle, "dbus_type_is_fixed");
   pdbus_get_version =
     dlsym(handle, "dbus_get_version");
   pdbus_pending_call_unref =
     dlsym(handle, "dbus_pending_call_unref");
   pdbus_pending_call_set_notify = 
     dlsym(handle, "dbus_pending_call_set_notify");
   pdbus_pending_call_steal_reply = 
     dlsym(handle, "dbus_pending_call_steal_reply");
   pdbus_pending_call_block = 
     dlsym(handle, "dbus_pending_call_block");
   return TCL_OK;
}

int Dbustcl_Init(Tcl_Interp *interp)
{
   Tcl_InitStubs(interp, "8.1", 0);
   if (handle == NULL)
     if (DBus_Load(interp) != TCL_OK) return TCL_ERROR;

   Tcl_MutexLock(&dbusMutex);
   if (!initialized) {
      Tcl_InitObjHashTable(&bus);
      Tcl_CreateEventSource(DBus_SetupProc, DBus_CheckProc, interp);
      initialized = TRUE;
   }
   Tcl_MutexUnlock(&dbusMutex);
   TclInitDBusCmd(interp);
   return Tcl_PkgProvide(interp, PACKAGE_NAME, PACKAGE_VERSION);
}

Tcl_DBusBus *DBus_GetConnection(Tcl_Interp *interp, Tcl_Obj *const name)
{
   Tcl_HashEntry *entry;
   Tcl_DBusBus *dbus;

   entry = Tcl_FindHashEntry(&bus, (char *) name);
   if (entry == NULL) return NULL;
   dbus = (Tcl_DBusBus *) Tcl_GetHashValue(entry);
   entry = Tcl_FindHashEntry(dbus->snoop, (char *) interp);
   if (entry != NULL)
     return dbus;
   else
     return NULL;
}

char *DBus_Alloc(int size, char *file, int line)
{
   char *rc;

   rc = Tcl_Alloc(size);
   printf("%p, %d bytes (%s:%d)\n", rc, size, file, line);
   return rc;
}

void DBus_Free(char *ptr, char *file, int line)
{
   printf("Free %p (%s:%d)\n", ptr, file, line);
   Tcl_Free(ptr);
}

int Tcl_CheckHashEmpty(Tcl_HashTable *hash)
{
   return (hash->numEntries == 0);
}

int DBus_SignalCleanup(Tcl_Interp *interp, Tcl_HashTable *members)
{
   Tcl_HashTable *interps;
   Tcl_HashEntry *memberPtr, *interpPtr;
   Tcl_HashSearch search;
   Tcl_DBusSignalData *signal;

   for (memberPtr = Tcl_FirstHashEntry(members, &search);
	memberPtr != NULL; memberPtr = Tcl_NextHashEntry(&search)) {
      interps = Tcl_GetHashValue(memberPtr);
      interpPtr = Tcl_FindHashEntry(interps, (char *) interp);
      if (interpPtr != NULL) {
	 signal = Tcl_GetHashValue(interpPtr);
	 Tcl_DecrRefCount(signal->script);
	 ckfree((char *) signal);
	 Tcl_DeleteHashEntry(interpPtr);
	 if (Tcl_CheckHashEmpty(interps)) {
	    Tcl_DeleteHashTable(interps);
	    ckfree((char *) interps);
	    Tcl_DeleteHashEntry(memberPtr);
	 }
      }
   }
   return Tcl_CheckHashEmpty(members);
}

int DBus_MethodCleanup(Tcl_Interp *interp, Tcl_HashTable *members)
{
   Tcl_HashEntry *memberPtr;
   Tcl_HashSearch search;
   Tcl_DBusMethodData *method;

   for (memberPtr = Tcl_FirstHashEntry(members, &search);
	memberPtr != NULL; memberPtr = Tcl_NextHashEntry(&search)) {
      method = Tcl_GetHashValue(memberPtr);
      if (method->interp == interp) {
	 Tcl_DecrRefCount(method->script);
	 ckfree((char *) method);
	 Tcl_DeleteHashEntry(memberPtr);
      }
   }
   return Tcl_CheckHashEmpty(members);
}

int DBus_HandlerCleanup(Tcl_Interp *interp, Tcl_DBusHandlerData *data)
{
   if (data->signal != NULL) {
      if (DBus_SignalCleanup(interp, data->signal)) {
	 Tcl_DeleteHashTable(data->signal);
	 ckfree((char *) data->signal);
	 data->signal = NULL;
      }
   }
   if (data->method != NULL) {
      if (DBus_MethodCleanup(interp, data->method)) {
	 Tcl_DeleteHashTable(data->method);
	 ckfree((char *) data->method);
	 data->method = NULL;
      }
   }
   return (data->signal == NULL && data->method == NULL);
}

void DBus_InterpCleanup(Tcl_Interp *interp, DBusConnection *conn, char *path)
{
   char **entries, **entry, *newpath, *pathentry;
   Tcl_DBusHandlerData *data;

   (*pdbus_connection_get_object_path_data)(conn, path, (void **)&data);
   if (data != NULL) {
      if (DBus_HandlerCleanup(interp, data)) {
	 (*pdbus_connection_unregister_object_path)(conn, path);
	 ckfree((char *)data);
      }
   }
   (*pdbus_connection_list_registered)(conn, path, &entries);
   if (*entries != NULL) {
      newpath = ckalloc(strlen(path) + 256);
      strcpy(newpath, path);
      pathentry = newpath + strlen(path) - 1;
      if (*pathentry++ != '/') *pathentry++ = '/';
      for (entry = entries; *entry != NULL; entry++) {
	 strncpy(pathentry, *entry, 255);
	 /* Get a list of descendents from the child */
	 DBus_InterpCleanup(interp, conn, newpath);
      }
      ckfree(newpath);
   }
   /* Release the entries array */
   (*pdbus_free_string_array)(entries);
}

const char *DBus_InterpPath(Tcl_Interp *interp)
{
   Tcl_Interp *master;
   master = Tcl_GetMaster(interp);
   if (master == NULL) return "";
   Tcl_GetInterpPath(master, interp);
   return (Tcl_GetStringResult(master));
}

void DBus_Disconnect(Tcl_Interp *interp, Tcl_HashEntry *busPtr)
{
   Tcl_DBusBus *data;
   Tcl_HashEntry *hPtr;
   Tcl_DBusSignalData *snoop;
  
   data = Tcl_GetHashValue(busPtr);
   /* Find all paths with handlers registered by the interp */
   DBus_InterpCleanup(interp, data->conn, "/");
   /* Find all handlers of the interp without a path */
   if (data->fallback != NULL) {
      if (DBus_HandlerCleanup(interp, data->fallback)) {
	 ckfree((char *)data->fallback);
	 data->fallback = NULL;
      }
   }
   /* Find snoop handlers */
   hPtr = Tcl_FindHashEntry(data->snoop, (char *) interp);
   if (hPtr != NULL) {
      snoop = Tcl_GetHashValue(hPtr);
      if (snoop->script != NULL)
	Tcl_DecrRefCount(snoop->script);
      ckfree((char *) snoop);
      Tcl_DeleteHashEntry(hPtr);
      if (Tcl_CheckHashEmpty(data->snoop)) {
	 Tcl_DeleteHashTable(data->snoop);
	 ckfree((char *) data->snoop);
	 if (data->type == N_BUS_TYPES)
	   (*pdbus_connection_unref)(data->conn);
	 ckfree((char *) data);
	 if (defaultbus == data) defaultbus = NULL;
	 Tcl_DeleteHashEntry(busPtr);
      }
   }
}

void DBus_InterpDelete(ClientData clientData, Tcl_Interp *interp)
{
   DBus_Disconnect(interp, (Tcl_HashEntry *) clientData);
}
