/* dbus-arch-deps.h */
typedef short dbus_int16_t;
typedef unsigned short dbus_uint16_t;
typedef int dbus_int32_t;
typedef unsigned int dbus_uint32_t;
#ifdef TCL_WIDE_INT_IS_LONG
#   undef TCL_WIDE_INT_TYPE
#   define TCL_WIDE_INT_TYPE long
#endif
typedef TCL_WIDE_INT_TYPE dbus_int64_t;
typedef unsigned TCL_WIDE_INT_TYPE dbus_uint64_t;

/* dbus-types.h */
typedef dbus_uint32_t  dbus_unichar_t;
typedef dbus_uint32_t  dbus_bool_t;

/* dbus-macros.h */
#ifndef TRUE
#  define TRUE 1
#endif
#ifndef FALSE
#  define FALSE 0
#endif

/* dbus-shared.h */
typedef enum {
    DBUS_BUS_SESSION,
    DBUS_BUS_SYSTEM,
    DBUS_BUS_STARTER
} DBusBusType;

typedef enum {
    DBUS_HANDLER_RESULT_HANDLED,
    DBUS_HANDLER_RESULT_NOT_YET_HANDLED,
    DBUS_HANDLER_RESULT_NEED_MEMORY
} DBusHandlerResult;

#define DBUS_SERVICE_DBUS	"org.freedesktop.DBus"
#define DBUS_PATH_DBUS		"/org/freedesktop/DBus"
#define DBUS_PATH_LOCAL		"/org/freedesktop/DBus/Local"

#define DBUS_NAME_FLAG_ALLOW_REPLACEMENT 0x1
#define DBUS_NAME_FLAG_REPLACE_EXISTING  0x2
#define DBUS_NAME_FLAG_DO_NOT_QUEUE      0x4

#define DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER  1
#define DBUS_REQUEST_NAME_REPLY_IN_QUEUE       2
#define DBUS_REQUEST_NAME_REPLY_EXISTS         3
#define DBUS_REQUEST_NAME_REPLY_ALREADY_OWNER  4

#define DBUS_RELEASE_NAME_REPLY_RELEASED        1
#define DBUS_RELEASE_NAME_REPLY_NON_EXISTENT    2
#define DBUS_RELEASE_NAME_REPLY_NOT_OWNER       3

/* dbus-message.h */
typedef struct DBusMessage DBusMessage;
typedef struct DBusMessageIter DBusMessageIter;
struct DBusMessageIter
{ 
    void *dummy1;
    void *dummy2;
    dbus_uint32_t dummy3;
    int dummy4;
    int dummy5;
    int dummy6;
    int dummy7;
    int dummy8;
    int dummy9;
    int dummy10;
    int dummy11;
    int pad1;
    int pad2;
    void *pad3;
};

/* dbus-memory.h */
typedef void (*DBusFreeFunction)(void *);

/* dbus-connection.h */
typedef struct DBusWatch DBusWatch;
typedef struct DBusTimeout DBusTimeout;
typedef struct DBusPendingCall DBusPendingCall;
typedef struct DBusConnection DBusConnection;
typedef struct DBusObjectPathVTable DBusObjectPathVTable;

typedef enum {
    DBUS_WATCH_READABLE = 1 << 0,
    DBUS_WATCH_WRITABLE = 1 << 1,
    DBUS_WATCH_ERROR    = 1 << 2,
    DBUS_WATCH_HANGUP   = 1 << 3
} DBusWatchFlags;

typedef enum {
    DBUS_DISPATCH_DATA_REMAINS,
    DBUS_DISPATCH_COMPLETE,
    DBUS_DISPATCH_NEED_MEMORY
} DBusDispatchStatus;

typedef dbus_bool_t (*DBusAddTimeoutFunction)(DBusTimeout*, void*);
typedef void (*DBusTimeoutToggledFunction)(DBusTimeout*, void*);
typedef void (*DBusRemoveTimeoutFunction)(DBusTimeout*, void*);
typedef void (*DBusPendingCallNotifyFunction)(DBusPendingCall*, void*);

typedef void (*DBusObjectPathUnregisterFunction)(DBusConnection*, void*);
typedef DBusHandlerResult (*DBusObjectPathMessageFunction)
  (DBusConnection*, DBusMessage*, void*);

struct DBusObjectPathVTable {
  DBusObjectPathUnregisterFunction   unregister_function;
  DBusObjectPathMessageFunction      message_function;
  void (* dbus_internal_pad1) (void *);
  void (* dbus_internal_pad2) (void *);
  void (* dbus_internal_pad3) (void *);
  void (* dbus_internal_pad4) (void *);
};

/* dbus-errors.h */
typedef struct DBusError DBusError;

struct DBusError {
    const char *name;
    const char *message;
    unsigned int dummy1 : 1;
    unsigned int dummy2 : 1;
    unsigned int dummy3 : 1;
    unsigned int dummy4 : 1;
    unsigned int dummy5 : 1;
    void *padding1;
};

/* dbus-protocol.h */
#define DBUS_TYPE_INVALID       ((int) '\0')
#define DBUS_TYPE_BYTE          ((int) 'y')
#define DBUS_TYPE_BOOLEAN       ((int) 'b')
#define DBUS_TYPE_INT16         ((int) 'n')
#define DBUS_TYPE_UINT16        ((int) 'q')
#define DBUS_TYPE_INT32         ((int) 'i')
#define DBUS_TYPE_UINT32        ((int) 'u')
#define DBUS_TYPE_INT64         ((int) 'x')
#define DBUS_TYPE_UINT64        ((int) 't')
#define DBUS_TYPE_DOUBLE        ((int) 'd')
#define DBUS_TYPE_STRING        ((int) 's')
#define DBUS_TYPE_OBJECT_PATH   ((int) 'o')
#define DBUS_TYPE_SIGNATURE     ((int) 'g')
#define DBUS_TYPE_ARRAY         ((int) 'a')
#define DBUS_TYPE_VARIANT       ((int) 'v')
#define DBUS_TYPE_STRUCT        ((int) 'r')
#define DBUS_TYPE_DICT_ENTRY    ((int) 'e')
#define DBUS_STRUCT_BEGIN_CHAR   ((int) '(')
#define DBUS_STRUCT_END_CHAR     ((int) ')')
#define DBUS_DICT_ENTRY_BEGIN_CHAR   ((int) '{')
#define DBUS_DICT_ENTRY_END_CHAR     ((int) '}')
#define DBUS_MAXIMUM_NAME_LENGTH 255

#define DBUS_MESSAGE_TYPE_INVALID       0
#define DBUS_MESSAGE_TYPE_METHOD_CALL   1
#define DBUS_MESSAGE_TYPE_METHOD_RETURN 2
#define DBUS_MESSAGE_TYPE_ERROR         3
#define DBUS_MESSAGE_TYPE_SIGNAL        4

#define DBUS_ERROR_FAILED	"org.freedesktop.DBus.Error.Failed"

/* dbus-signature.h */
typedef struct { 
    void *dummy1;
    void *dummy2;
    dbus_uint32_t dummy8;
    int dummy12;
    int dummy17;
} DBusSignatureIter;

/* dbus-bus.h */
extern DBusConnection* (*pdbus_bus_get)(DBusBusType, DBusError*);
extern dbus_bool_t (*pdbus_bus_register)(DBusConnection*, DBusError*);
extern const char* (*pdbus_bus_get_unique_name)(DBusConnection*);
extern int (*pdbus_bus_request_name)
  (DBusConnection*, const char*, unsigned int, DBusError*);
extern int (*pdbus_bus_release_name)
  (DBusConnection*, const char*, DBusError*);
extern void (*pdbus_bus_add_match)
  (DBusConnection*, const char*, DBusError*);
extern void (*pdbus_bus_remove_match)
  (DBusConnection*, const char*, DBusError*);

/* dbus-connection.h */
extern DBusConnection* (*pdbus_connection_open)(const char*, DBusError*);
extern void (*pdbus_connection_unref)(DBusConnection*);
extern char* (*pdbus_connection_get_server_id)(DBusConnection*);
extern void (*pdbus_connection_flush)(DBusConnection*);
extern dbus_bool_t (*pdbus_connection_read_write)(DBusConnection*, int);
extern DBusDispatchStatus (*pdbus_connection_get_dispatch_status)
  (DBusConnection*);
extern DBusDispatchStatus (*pdbus_connection_dispatch)
  (DBusConnection*);
extern dbus_bool_t (*pdbus_connection_has_messages_to_send)(DBusConnection*);
extern dbus_bool_t (*pdbus_connection_send)
  (DBusConnection*, DBusMessage*, dbus_uint32_t*);
extern dbus_bool_t (*pdbus_connection_send_with_reply)
  (DBusConnection*, DBusMessage*, DBusPendingCall**, int);
extern dbus_bool_t (*pdbus_connection_set_timeout_functions)
  (DBusConnection*, DBusAddTimeoutFunction, DBusRemoveTimeoutFunction, 
   DBusTimeoutToggledFunction, void*, DBusFreeFunction);
extern dbus_bool_t (*pdbus_connection_register_object_path)
  (DBusConnection*, const char*, const DBusObjectPathVTable*, void*);
extern dbus_bool_t (*pdbus_connection_register_fallback)
  (DBusConnection*, const char*, const DBusObjectPathVTable*, void*);
extern dbus_bool_t (*pdbus_connection_unregister_object_path)
  (DBusConnection*, const char*);
extern dbus_bool_t (*pdbus_connection_get_object_path_data)
  (DBusConnection*, const char*, void**);
extern dbus_bool_t (*pdbus_connection_list_registered)
  (DBusConnection*, const char*, char***);
extern int (*pdbus_timeout_get_interval)(DBusTimeout*);
extern void* (*pdbus_timeout_get_data)(DBusTimeout*);
extern void (*pdbus_timeout_set_data)(DBusTimeout*, void*, DBusFreeFunction);
extern dbus_bool_t (*pdbus_timeout_handle)(DBusTimeout*);

/* dbus-errors.h */
extern void (*pdbus_error_init)(DBusError*);
extern void (*pdbus_error_free)(DBusError*);
extern dbus_bool_t (*pdbus_error_is_set)(const DBusError*);

/* dbus-message.h */
extern DBusMessage* (*pdbus_message_new)(int);
extern DBusMessage* (*pdbus_message_new_method_call)
  (const char*, const char*, const char*, const char*);
extern DBusMessage* (*pdbus_message_ref)(DBusMessage*);
extern void (*pdbus_message_unref)(DBusMessage*);
extern int (*pdbus_message_get_type)(DBusMessage*);
extern dbus_bool_t (*pdbus_message_set_path)(DBusMessage*, const char*);
extern const char* (*pdbus_message_get_path)(DBusMessage*);
extern dbus_bool_t (*pdbus_message_set_interface)(DBusMessage*, const char*);
extern const char* (*pdbus_message_get_interface)(DBusMessage*);
extern dbus_bool_t (*pdbus_message_set_member)(DBusMessage*, const char*);
extern const char* (*pdbus_message_get_member)(DBusMessage*);
extern dbus_bool_t (*pdbus_message_set_error_name)(DBusMessage*, const char*);
extern const char* (*pdbus_message_get_error_name)(DBusMessage*);
extern dbus_bool_t (*pdbus_message_set_destination)(DBusMessage*, const char*);
extern const char* (*pdbus_message_get_destination)(DBusMessage*);
extern dbus_bool_t (*pdbus_message_set_sender)(DBusMessage*, const char*);
extern const char* (*pdbus_message_get_sender)(DBusMessage*);
extern const char* (*pdbus_message_get_signature)(DBusMessage*);
extern void (*pdbus_message_set_no_reply)(DBusMessage*, dbus_bool_t);
extern dbus_bool_t (*pdbus_message_get_no_reply)(DBusMessage*);
extern dbus_uint32_t (*pdbus_message_get_serial)(DBusMessage*);
extern void (*pdbus_message_set_serial)(DBusMessage*, dbus_uint32_t);
extern dbus_bool_t (*pdbus_message_set_reply_serial)
  (DBusMessage*, dbus_uint32_t);
extern dbus_uint32_t (*pdbus_message_get_reply_serial)(DBusMessage*);
extern void (*pdbus_message_set_auto_start)(DBusMessage*, dbus_bool_t);
extern dbus_bool_t (*pdbus_message_get_auto_start)(DBusMessage*);
extern dbus_bool_t (*pdbus_message_append_args)(DBusMessage*, int, ...);
extern dbus_bool_t (*pdbus_message_iter_init)(DBusMessage*, DBusMessageIter*);
extern dbus_bool_t (*pdbus_message_iter_next)(DBusMessageIter*);
extern char* (*pdbus_message_iter_get_signature)(DBusMessageIter*);
extern int (*pdbus_message_iter_get_arg_type)(DBusMessageIter*);
extern int (*pdbus_message_iter_get_element_type)(DBusMessageIter*);
extern void (*pdbus_message_iter_recurse)(DBusMessageIter*, DBusMessageIter*);
extern void (*pdbus_message_iter_get_basic)(DBusMessageIter*, void*);
extern void (*pdbus_message_iter_get_fixed_array)
  (DBusMessageIter*, void*, int*);
extern void (*pdbus_message_iter_init_append)(DBusMessage*, DBusMessageIter*);
extern dbus_bool_t (*pdbus_message_iter_append_basic)
  (DBusMessageIter*, int, const void*);
extern dbus_bool_t (*pdbus_message_iter_open_container)
  (DBusMessageIter*, int, const char*, DBusMessageIter*);
extern dbus_bool_t (*pdbus_message_iter_close_container)
  (DBusMessageIter*, DBusMessageIter*);
extern const char* (*pdbus_message_type_to_string)(int);

/* dbus-misc.h */
extern char* (*pdbus_get_local_machine_id)(void);
extern void (*pdbus_get_version)(int*, int*, int*);

/* dbus-memory.h */
extern void (*pdbus_free)(void*);
extern void (*pdbus_free_string_array)(char**);

/* dbus-signature.h */
extern void (*pdbus_signature_iter_init)(DBusSignatureIter*, const char*);
extern int (*pdbus_signature_iter_get_current_type)(const DBusSignatureIter*);
extern char* (*pdbus_signature_iter_get_signature)(const DBusSignatureIter*);
extern dbus_bool_t (*pdbus_signature_iter_next)(DBusSignatureIter*);
extern void (*pdbus_signature_iter_recurse)
  (const DBusSignatureIter*, DBusSignatureIter*);
extern dbus_bool_t (*pdbus_signature_validate)(const char*, DBusError*);
extern dbus_bool_t (*pdbus_signature_validate_single)(const char*, DBusError*);
extern dbus_bool_t (*pdbus_type_is_fixed)(int);

/* dbus-pending-call.h */
extern void (*pdbus_pending_call_unref)(DBusPendingCall*);
extern dbus_bool_t (*pdbus_pending_call_set_notify)
  (DBusPendingCall*, DBusPendingCallNotifyFunction, void*, DBusFreeFunction);
extern DBusMessage* (*pdbus_pending_call_steal_reply)(DBusPendingCall*);
extern void (*pdbus_pending_call_block)(DBusPendingCall*);
